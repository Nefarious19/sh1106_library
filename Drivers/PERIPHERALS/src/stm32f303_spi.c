/*
 * stm32f303_spi.c
 *
 *  Created on: 11 Sep 2019
 *      Author: rato
 */
#include <stm32f3xx.h>


void SPI_Init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN; //Enable SPI1
	SPI1->CR1 = SPI_CR1_BIDIMODE |		//Only one line of data
				SPI_CR1_BIDIOE   |  	//Enable Transmission only
				SPI_CR1_SSM      |  	//NSS software management
				SPI_CR1_SSI      |  	//Set internal SSI
				SPI_CR1_MSTR     |  	//SPI in MASTER mode
				SPI_CR1_BR_0     |
				SPI_CR1_BR_1     |  	//Prescaler == 2, SCK = 36MHZ / 2 = 18MHz
				SPI_CR1_SPE;        	//Enable SPI
}

void SPI_Deinit(void)
{
	SPI1->CR1 = 0;
	SPI1->CR2 = 0;
	SPI1->SR = 0;
	RCC->APB2ENR &= ~RCC_APB2ENR_SPI1EN;
}


void SPI_writeData(uint8_t data)
{
	while(!(SPI1->SR & SPI_SR_TXE)); //wait until tx is empty again
	SPI1->DR = data;                 //put data into TX data register
}

void SPI_writeNData(uint8_t * data, uint8_t len)
{
	while(len--)
	{
		while(!(SPI1->SR & SPI_SR_TXE)); //wait until tx is empty again
		SPI1->DR = *data++;                 //put data into TX data register
	}
	while((SPI1->SR & SPI_SR_BSY)); //wait until tx is empty again

}
