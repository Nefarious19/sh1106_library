/*
 * stm32f303_spi.h
 *
 *  Created on: 11 Sep 2019
 *      Author: rato
 */

#ifndef PERIPHERALS_INC_STM32F303_SPI_H_
#define PERIPHERALS_INC_STM32F303_SPI_H_

void SPI_Init(void);
void SPI_Deinit(void);
void SPI_writeNData(uint8_t * data, uint8_t len);
void SPI_writeData(uint8_t data);

#endif /* PERIPHERALS_INC_STM32F303_SPI_H_ */
