
#include <stm32f3xx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../Drivers/PERIPHERALS/inc/stm32f303_uart.h"
#include "../Drivers/PERIPHERALS/inc/stm32f303_i2c.h"
#include "../Drivers/PERIPHERALS/inc/stm32f303_spi.h"
#include "../BME280/bme280_defs.h"
#include "utils.h"

#include "../SH1106/inc/oled.h"

volatile uint32_t timer = 0;

void SysTick_Handler (void)
{
	if(timer) timer --;
}

PERIPH_USART_Instance_t usart1 = {0};

void USART1_RX_Callback (void)
{
	char * ptr = 0;
	ptr = malloc(PERIPH_USART_CIRCULLAR_BUFFER_RX_SIZE);
	memset(ptr, 0, PERIPH_USART_CIRCULLAR_BUFFER_RX_SIZE);
	PERIPH_USART_GetString(&usart1,ptr);
	PERIPH_USART_puts(&usart1, ptr);
	free(ptr);
}

void i2c_cllback (uint8_t address)
{
	printf("Device found @ addr: 0x%x\r\n", address);
}

uint8_t buffer[0x5E] = { 0 };
void spi_test (void);
int main(void)
{



  RCC_Init();
  GPIO_init();
  SysTick_Config(72000000UL / 1000);
  PERIPH_USART_Init(USART1, &usart1, 115200);
  PERIPH_USART_RegisterRxCallback(&usart1,USART1_RX_Callback);

  I2C_init();
  SPI_Init();
//  OLED_CommunicationInterface_t i2cForOled;
//  i2cForOled.OLED_InterfaceWriteNBytes = I2C_writeNBytes;
//  OLED_init(&i2cForOled);

  OLED_CommunicationInterface_t spiOled;
  spiOled.OLED_InterfaceWriteNBytes = SPI_writeNData;
  spiOled.OLED_

  OLED_GFX_drawRoundedRect(0,0, OLED_WIDTH - 1, OLED_HEIGHT - 1, 5, OLED_COLOR_WHITE);
  OLED_PRT_putStr("Witam",10,10,1);
  OLED_PRT_putStr("ekipe",10,20,1);
  OLED_PRT_putStr("Sunduino!",10,30,1);
  OLED_updateScreen();
  while (1)
  {
	  PERIPH_USART_RXService();

	  if(!timer)
	  {
		  static uint32_t shift = 0x0100;
		  timer = 100;
		  GPIOE->ODR &= 0x00FF;
		  GPIOE->ODR |= shift;
		  shift <<= 1;
		  if(shift == 0x10000) shift = 0x0100;
		  spi_test();
	  }


  }
}


void spi_test (void)
{
	uint8_t buf[10];
	memset(buf,0,10);
	SPI_csLow();
	SPI_writeNData(buf,10);
	SPI_csHigh();
}


