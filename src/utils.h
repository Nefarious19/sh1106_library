/*
 * utils.h
 *
 *  Created on: 4 Sep 2019
 *      Author: rato
 */

#ifndef UTILS_H_
#define UTILS_H_

int _write(int file, char *data, int len);
void GPIO_init();
void RCC_Init(void);
void SPI_csLow(void);
void SPI_csHigh(void);
void SPI_resetLow(void);
void SPI_resetHigh(void);
void SPI_cdLow(void);
void SPI_cdHIGH(void);

#endif /* UTILS_H_ */
